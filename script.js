import data from '/data.js';

let numberColumn, redCell;
let startNumber = 0;
let rowsPerPage = 10;
let page = 0;
let dataset = "audio_dataset";
let type = "audio_data";
let windowWidth = window.innerWidth;
let initialValue = data[dataset]["data"][type];
let titles = initialValue.titles.length;
let count = initialValue.data.length;

window.addEventListener('resize', function(e) {
    windowWidth = window.innerWidth;
    numberColumn = windowWidth < 1190 ? 3 : titles;
    startNumber = windowWidth < 1190 ? 1 : 0;
    redCell = windowWidth < 1190 ? 0 : 1;
    createDatasetTable(type, dataset);
}, true);

const createDatasetTable = (typeOfdata, value=dataset) => {
    type = typeOfdata;
    dataset = value ? value : dataset;
    const datasetData = data[dataset]["data"][typeOfdata];
    count = datasetData.data.length;
    page = page + 1 > Math.ceil(count / rowsPerPage) ? 0 : page;
    let datasetDiv = document.querySelector("div.dataset-container")
    while (datasetDiv.firstChild) datasetDiv.removeChild(datasetDiv.firstChild)
    let datasetTable = document.createElement('table');

    datasetTable.id = "datasets";

    let datasetTableHead = document.createElement("thead");

    datasetTableHead.className = "datasets-head";

    let datasetTableHeaderRow = document.createElement("tr");

    datasetTableHeaderRow.className = "datasets-head_row";

    if (windowWidth < 1190) {
        let th = document.createElement("th");
        datasetTableHeaderRow.append(th);
    };

    datasetData.titles.slice(startNumber, numberColumn).forEach((title) => {
        let datasetHeader = document.createElement("th");
        datasetHeader.innerText = title;
        datasetHeader.className = "medium-text";
        datasetTableHeaderRow.append(datasetHeader);
    });

    datasetTableHead.append(datasetTableHeaderRow);

    datasetTable.append(datasetTableHead);

    let datasetTableBody = document.createElement("tbody");

    datasetTableBody.className = "datasets-body medium-text";

    datasetTable.append(datasetTableBody);

    datasetData.data
    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .forEach((row, index) => {
        let expandPart;
        const first = createMainPartRowTable(row, startNumber, numberColumn, index + 1);
        datasetTableBody.append(first);                                                                                                                                                                                                                                                                                                                                                                                                                                              
        if (windowWidth < 1190) {
            expandPart = createExpandPartRowTable(row, numberColumn, Object.keys(row).length, index + 1);
            datasetTableBody.append(expandPart);
        };
    });
    datasetDiv.append(datasetTable);
    updatePagination();
};

const createMainPartRowTable = (value, start, end, indexData) => {
    const position = `hidden_row${indexData}`;
    let datasetTableBodyRow = document.createElement("tr");
    if(windowWidth < 1190) {
        const imgSource ="assets/icons/expand_down_icon.svg";
        let tdata = document.createElement("td");
        let icon = document.createElement("img");
        icon.src = imgSource;
        tdata.append(icon);
        datasetTableBodyRow.append(tdata);
        tdata.addEventListener("click", () => {
            icon.classList.toggle("rotation-expand-icon");
        });
        icon.addEventListener("click", () => {
            showHideRow(position);
        });
    }
    Object.keys(value).slice(start, end).forEach((key, index) => {
        let rowData = document.createElement("td");
        if (index === redCell) rowData.className = "red-text";
        rowData.innerText = value[key];
        rowData.classList.add("medium-text");
        datasetTableBodyRow.append(rowData);
    });
    return datasetTableBodyRow;
};

const createExpandPartRowTable = (value, start, end, index) => {
    let trow = document.createElement("tr");
    let td = document.createElement("td");
    td.colSpan = "3";
    trow.id = `hidden_row${index}`
    trow.className = "expand-row hidden_row";
    Object.keys(value).slice(start, end).forEach((key) => {
        let span = document.createElement("span");
        span.innerText = `${key}: ${value[key]}` + " ";
        td.append(span);
    });
    trow.append(td);
    return trow;
};


const changeDatasetData = (e, typeOfdataset, dataset) => {
    page = 0;
    const btns = document.querySelectorAll("div.dataset-group_button div");
    btns.forEach((btn) => {
        btn.classList.remove("btn-active");
        btn.classList.remove("btn-non_active");
    });
    e.classList.add("btn-active");
    const listTypeOfDataset = document.querySelector('.dataset-selection');
    while (listTypeOfDataset.firstChild) listTypeOfDataset.removeChild(listTypeOfDataset.firstChild);
    data[dataset].types.forEach(type => {
        const option = document.createElement('option');
        const value = type.toLowerCase().replaceAll(' ', '_');
        option.innerText = type;
        option.value = value;
        listTypeOfDataset.append(option);
    });
    createDatasetTable(typeOfdataset, dataset);
};

const onPageChange = (event, newPage) => {
    page = newPage;
    createDatasetTable(type, dataset);
};

const handleBackButtonClick = (event) => {
    if(page === 0) return;
    onPageChange(event, page - 1);
};

const handleNextButtonClick = (event) => {
    if(page >= Math.ceil(count / rowsPerPage) - 1) {
        const nextBtn = document.querySelector("#nextButton");
        nextBtn.classList.add = "hidden_row";
        return
    };
    onPageChange(event, page + 1);
};

const updatePagination = () => {
    const numberPage = document.querySelector("#numberPage");
    numberPage.innerText = `${page + 1}/${Math.ceil(count / rowsPerPage)}`;
};

createDatasetTable(type, dataset);

window.createDatasetTable = createDatasetTable;
window.changeDatasetData = changeDatasetData;
window.handleBackButtonClick = handleBackButtonClick;
window.handleNextButtonClick = handleNextButtonClick;

