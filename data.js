const data = {
   "audio_dataset":{
      "id":1,
      "label":"Audio dataset",
      "types":[
         "Audio data",
         "Medical Audio data"
      ],
      "data":{
         "audio_data":{
            "titles":[
               "Sr no",
               "Type of data",
               "Language",
               "Quantity",
               "Description",
               "Format"
            ],
            "data":[
               {
                  "id":1,
                  "Type of data":"Audio with script",
                  "Language":"tl-FIL",
                  "Quantity":"500 hrs",
                  "Description":"H2H, Synthetic call center",
                  "Format":"Wav, Stereo channel"
               },
               {
                  "id":2,
                  "Type of data":"Audio with script",
                  "Language":"it-IT",
                  "Quantity":"501 hrs",
                  "Description":"H2H, Real call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":3,
                  "Type of data":"Audio with script",
                  "Language":"en-US",
                  "Quantity":"40 hrs",
                  "Description":"Daily conversation, standard accent.",
                  "Format":"Wav, Stereo channel"
               },
               {
                  "id":4,
                  "Type of data":"Audio with script",
                  "Language":"es-MX",
                  "Quantity":"40 hrs",
                  "Description":"H2H, Synthetic call center",
                  "Format":"Wav, Stereo channel"
               },
               {
                  "id":5,
                  "Type of data":"Audio with script",
                  "Language":"en-cy-GB",
                  "Quantity":"60 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":6,
                  "Type of data":"Audio with script",
                  "Language":"en-GB- Irish",
                  "Quantity":"60 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":7,
                  "Type of data":"Audio with script",
                  "Language":"en- scotland",
                  "Quantity":"60 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":8,
                  "Type of data":"Audio with script",
                  "Language":"it-IT",
                  "Quantity":"150 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":9,
                  "Type of data":"Audio with script",
                  "Language":"de-DE",
                  "Quantity":"60 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":10,
                  "Type of data":"Audio with script",
                  "Language":"pl-PL",
                  "Quantity":"100 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":11,
                  "Type of data":"Audio with script",
                  "Language":"es-MX",
                  "Quantity":"100 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":12,
                  "Type of data":"Audio with script",
                  "Language":"Pt-Br",
                  "Quantity":"100 hrs",
                  "Description":"H2M, Synthetic call center",
                  "Format":"Wav, mono channel"
               },
               {
                  "id":13,
                  "Type of data":"Audio",
                  "Language":"En-AU",
                  "Quantity":"478 hrs",
                  "Description":"H2H, Real call center.",
                  "Format":"Wav mp3, mono channel"
               },
               {
                  "id":14,
                  "Type of data":"Audio",
                  "Language":"hi-IN",
                  "Quantity":"819 hrs",
                  "Description":"H2H, Real call center.",
                  "Format":"Wav mp3, mono channel"
               },
               {
                  "id":15,
                  "Type of data":"Audio with script",
                  "Language":"en-US",
                  "Quantity":"100 hrs",
                  "Description":"H2H, Real call center.",
                  "Format":"Wav mp3, mono channel"
               }
            ]
         },
         "medical_audio_data":{
            "titles":[
               "Sr no",
               "Type of data",
               "Language",
               "Quantity",
               "Description",
               "Format"
            ],
            "data":[
               {
                  "id":1,
                  "Type of data":"Audio",
                  "Language":"en-US",
                  "Quantity":"1000 hrs",
                  "Description":"Real doctor and patients conversation",
                  "Format":"Wav mp3, mono channel"
               },
               {
                  "id":2,
                  "Type of data":"Audio with script",
                  "Language":"en- standard",
                  "Quantity":"2000 hrs",
                  "Description":"Real Medical dictation audios, 3-12 mins audio",
                  "Format":"Wav mp3, mono channel"
               },
               {
                  "id":3,
                  "Type of data":"Audio",
                  "Language":"En-US",
                  "Quantity":"500 hrs",
                  "Description":"H2H, both real conversations selling medical products service",
                  "Format":"Wav mp3, mono channel"
               }
            ]
         }
      }
   },
   "ocr_dataset":{
      "id":2,
      "label":"OCR dataset",
      "types":[
         "Imaging data"
      ],
      "data":{
         "imaging_data":{
            "titles":[
               "Sr no",
               "Type of data",
               "Language",
               "Quantity",
               "Format"
            ],
            "data":[
               {
                  "id":1,
                  "Type of data":"Image",
                  "Language":"Handwriting texts, curved printed texts, detectable printed texts. Different categories (invoice, documents, storefront, receipt, etc)",
                  "Quantity":"ko-KR, ja-JP, ru-RU, vi-VN, zh-CN",
                  "Format":"Heic, mov (pair)"
               },
               {
                  "id":2,
                  "Type of data":"Image",
                  "Language":"Printed texts of around 20 categories. (documents, inv, table, receipts, menus, etc)",
                  "Quantity":"it-IT",
                  "Format":"JPEG"
               },
               {
                  "id":3,
                  "Type of data":"Image",
                  "Language":"Bank statement, pay slip, cheques",
                  "Quantity":"En",
                  "Format":"JPEG"
               },
               {
                  "id":4,
                  "Type of data":"Image",
                  "Language":"Table, invoice and general categories",
                  "Quantity":"Zh-CN",
                  "Format":"JPEG"
               }
            ]
         }
      }
   },
   "medical_dataset":{
      "id":3,
      " label":"Medical dataset",
      "types":[
         "Imaging data",
         "Audio data"
      ],
      "data":{
         "imaging_data":{
            "titles":[
               "Sr no",
               "Body Part",
               "Quantity",
               "Speciality",
               "Format"
            ],
            "data":[
               {
                  "id":1,
                  "Body_Part":"BRAIN",
                  "Quantity":3618,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":2,
                  "Body_Part":"SKULL",
                  "Quantity":9,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":3,
                  "Body_Part":"HEAD",
                  "Quantity":53144,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":4,
                  "Body_Part":"NECK",
                  "Quantity":25,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":5,
                  "Body_Part":"CHEST",
                  "Quantity":29,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":6,
                  "Body_Part":"ABDOMEN",
                  "Quantity":316,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":7,
                  "Body_Part":"CSPINE",
                  "Quantity":726,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":8,
                  "Body_Part":"TSPINE",
                  "Quantity":9,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":9,
                  "Body_Part":"LSPINE",
                  "Quantity":7287,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":10,
                  "Body_Part":"LIVER",
                  "Quantity":16,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":11,
                  "Body_Part":"SHOULDER",
                  "Quantity":150,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":12,
                  "Body_Part":"WRIST",
                  "Quantity":11,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":13,
                  "Body_Part":"KNEE",
                  "Quantity":1720,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":14,
                  "Body_Part":"LEG",
                  "Quantity":5,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":15,
                  "Body_Part":"ANKLE",
                  "Quantity":10,
                  "Speciality":"MRI",
                  "Format":"Dicom"
               },
               {
                  "id":16,
                  "Body_Part":"BRAIN",
                  "Quantity":24804,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":17,
                  "Body_Part":"SKULL",
                  "Quantity":7,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":18,
                  "Body_Part":"HEAD",
                  "Quantity":41916,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":19,
                  "Body_Part":"NECK",
                  "Quantity":460,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":20,
                  "Body_Part":"CHEST",
                  "Quantity":44626,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":21,
                  "Body_Part":"ABDOMEN",
                  "Quantity":38371,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":22,
                  "Body_Part":"CSPINE",
                  "Quantity":112,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":23,
                  "Body_Part":"TSPINE",
                  "Quantity":22,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":24,
                  "Body_Part":"LSPINE",
                  "Quantity":637,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":25,
                  "Body_Part":"LIVER",
                  "Quantity":248,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":26,
                  "Body_Part":"SHOULDER",
                  "Quantity":105,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":27,
                  "Body_Part":"WRIST",
                  "Quantity":88,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":28,
                  "Body_Part":"KNEE",
                  "Quantity":16,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":29,
                  "Body_Part":"ANKLE",
                  "Quantity":5,
                  "Speciality":"CT",
                  "Format":"Dicom"
               },
               {
                  "id":30,
                  "Body_Part":"SKULL",
                  "Quantity":166976,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":31,
                  "Body_Part":"HEAD",
                  "Quantity":42,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":32,
                  "Body_Part":"NECK",
                  "Quantity":66,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":33,
                  "Body_Part":"CHEST",
                  "Quantity":346698,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":34,
                  "Body_Part":"BREAST",
                  "Quantity":35869,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":35,
                  "Body_Part":"ABDOMEN",
                  "Quantity":35444,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":36,
                  "Body_Part":"CSPINE",
                  "Quantity":37954,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":37,
                  "Body_Part":"TSPINE",
                  "Quantity":2106,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":38,
                  "Body_Part":"LSPINE",
                  "Quantity":40944,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":39,
                  "Body_Part":"SHOULDER",
                  "Quantity":13742,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":40,
                  "Body_Part":"HAND",
                  "Quantity":7267,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":41,
                  "Body_Part":"WRIST",
                  "Quantity":1640,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":42,
                  "Body_Part":"KNEE",
                  "Quantity":34821,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":43,
                  "Body_Part":"LEG",
                  "Quantity":3589,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":44,
                  "Body_Part":"FEMUR",
                  "Quantity":342,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":45,
                  "Body_Part":"FOOT",
                  "Quantity":4840,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":46,
                  "Body_Part":"ANKLE",
                  "Quantity":7690,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":47,
                  "Body_Part":"FOREARM",
                  "Quantity":315,
                  "Speciality":"CR",
                  "Format":"Dicom"
               },
               {
                  "id":48,
                  "Body_Part":"LUNG",
                  "Quantity":1000,
                  "Speciality":"Dual",
                  "Format":"CT"
               },
               {
                  "id":49,
                  "Body_Part":"CHEST",
                  "Quantity":1000,
                  "Speciality":"HRCT",
                  "Format":"JPEG"
               },
               {
                  "id":50,
                  "Body_Part":"CHEST",
                  "Quantity":10000,
                  "Speciality":"XRAY",
                  "Format":"JPEG"
               },
               {
                  "id":51,
                  "Body_Part":"MAMMOGRAM",
                  "Quantity":3000,
                  "Speciality":"XRAY",
                  "Format":"JPEG"
               },
               {
                  "id":52,
                  "Body_Part":"BRAIN",
                  "Quantity":1000,
                  "Speciality":"NECT",
                  "Format":"Dicom"
               },
               {
                  "id":53,
                  "Body_Part":"LUNG, BREAST",
                  "Quantity":4000,
                  "Speciality":"USG",
                  "Format":"JPEG"
               }
            ]
         },
         "audio_data":{
            "titles":[
               "Sr no",
               "Type of data",
               "Language",
               "Quantity",
               "Description",
               "Format"
            ],
            "data":[
               {
                  "id":1,
                  "Type of data":"Audio",
                  "Language":"en-US",
                  "Quantity":"1000 hrs",
                  "Description":"Real doctorand patients’ conversation",
                  "Format":"Wavmp3,mono channel"
               },
               {
                  "id":2,
                  "Type of data":"Audio with script",
                  "Language":"en- standard",
                  "Quantity":"2000 hrs",
                  "Description":"Real Medical dictation audios, 3-12 mins audio",
                  "Format":"Wav mp3, mono channel"
               },
               {
                  "id":3,
                  "Type of data":"Audio",
                  "Language":"En-US",
                  "Quantity":"500 hrs",
                  "Description":"H2H, both real conversations selling medical products service",
                  "Format":"Wav mp3, mono channel"
               }
            ]
         }
      }
   }
}

export default data;